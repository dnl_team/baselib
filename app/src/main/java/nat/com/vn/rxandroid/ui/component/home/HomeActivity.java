package nat.com.vn.rxandroid.ui.component.home;

import android.support.annotation.VisibleForTesting;
import android.support.test.espresso.IdlingResource;

import javax.inject.Inject;

import nat.com.vn.rxandroid.App;
import nat.com.vn.rxandroid.R;
import nat.com.vn.rxandroid.ui.base.BaseActivity;

import static nat.com.vn.rxandroid.utils.EspressoIdlingResource.getIdlingResource;


public class HomeActivity extends BaseActivity implements HomeContract.View {

    @Inject
    HomePresenter presenter;

    @Override
    protected void initializeDagger() {
        App app = (App) getApplicationContext();
        app.getMainComponent().inject(HomeActivity.this);
    }

    @Override
    protected void initializePresenter() {
        super.presenter=presenter;
        presenter.setView(this);
    }


    @Override
    public int getLayoutId() {
        return R.layout.home_layout;
    }



    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @VisibleForTesting
    public IdlingResource getCountingIdlingResource() {
        return getIdlingResource();
    }
}
