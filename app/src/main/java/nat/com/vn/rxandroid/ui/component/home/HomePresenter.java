package nat.com.vn.rxandroid.ui.component.home;

import android.os.Bundle;

import javax.inject.Inject;

import nat.com.vn.rxandroid.ui.base.Presenter;


public class HomePresenter extends Presenter<HomeContract.View> implements HomeContract.Presenter {

    @Inject
    public HomePresenter() {
    }

    @Override
    public void initialize(Bundle extras) {
        super.initialize(extras);
    }


}
