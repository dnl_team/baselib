package nat.com.vn.rxandroid.di;


import javax.inject.Singleton;

import dagger.Component;
import nat.com.vn.rxandroid.ui.component.home.HomeActivity;
import nat.com.vn.rxandroid.ui.component.splash.SplashActivity;

@Singleton
@Component(modules = MainModule.class)
public interface MainComponent {
    void inject(SplashActivity activity);

    void inject(HomeActivity activity);
}
