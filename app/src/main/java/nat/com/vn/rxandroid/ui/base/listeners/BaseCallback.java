package nat.com.vn.rxandroid.ui.base.listeners;


public interface BaseCallback {
    void onSuccess(Object object);

    void onFail();
}
