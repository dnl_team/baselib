package nat.com.vn.rxandroid.data;

import javax.inject.Inject;

import nat.com.vn.rxandroid.data.local.LocalRepository;
import nat.com.vn.rxandroid.data.remote.RemoteRepository;


public class DataRepository implements DataSource {
    private RemoteRepository remoteRepository;
    private LocalRepository localRepository;

    @Inject
    public DataRepository(RemoteRepository remoteRepository, LocalRepository localRepository) {
        this.remoteRepository = remoteRepository;
        this.localRepository = localRepository;
    }
}
