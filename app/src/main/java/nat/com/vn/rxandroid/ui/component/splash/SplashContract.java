package nat.com.vn.rxandroid.ui.component.splash;

import nat.com.vn.rxandroid.ui.base.listeners.BaseView;

public interface SplashContract {
    interface View extends BaseView {
        void NavigateToMainScreen();
    }

    interface Presenter {

    }
}
