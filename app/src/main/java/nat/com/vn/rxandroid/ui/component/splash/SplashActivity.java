package nat.com.vn.rxandroid.ui.component.splash;

import android.content.Intent;
import android.os.Handler;

import javax.inject.Inject;

import nat.com.vn.rxandroid.App;
import nat.com.vn.rxandroid.R;
import nat.com.vn.rxandroid.ui.base.BaseActivity;
import nat.com.vn.rxandroid.ui.component.home.HomeActivity;

import static nat.com.vn.rxandroid.utils.Constants.SPLASH_DELAY;


public class SplashActivity extends BaseActivity implements SplashContract.View {

    @Inject
    SplashPresenter splashPresenter;

    @Override
    protected void initializeDagger() {
        App app = (App) getApplicationContext();
        app.getMainComponent().inject(SplashActivity.this);
    }

    @Override
    protected void initializePresenter() {
        super.presenter = splashPresenter;
        presenter.setView(this);
    }

    @Override
    public int getLayoutId() {
        return R.layout.splash_layout;
    }


    @Override
    public void NavigateToMainScreen() {
        new Handler().postDelayed(() -> {
            Intent intent = new Intent(SplashActivity.this, HomeActivity.class);
            startActivity(intent);
            finish();
        }, SPLASH_DELAY);
    }
}
