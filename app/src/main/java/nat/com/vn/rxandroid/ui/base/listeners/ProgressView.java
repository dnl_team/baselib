package nat.com.vn.rxandroid.ui.base.listeners;

public interface ProgressView {
    void showProgress(String message);

    void hideProgress();
}
