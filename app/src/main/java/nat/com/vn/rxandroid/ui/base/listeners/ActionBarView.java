package nat.com.vn.rxandroid.ui.base.listeners;

public interface ActionBarView {

    void setUpIconVisibility(boolean visible);

    void setTitle(String titleKey);

    void setSettingsIconVisibility(boolean visibility);

    void setRefreshVisibility(boolean visibility);
}
