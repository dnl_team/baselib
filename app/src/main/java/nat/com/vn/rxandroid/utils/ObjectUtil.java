package nat.com.vn.rxandroid.utils;
import android.text.TextUtils;
import java.util.HashMap;
import java.util.List;

public class ObjectUtil {
    public static boolean isEmpty(String string) {
        return TextUtils.isEmpty(string);
    }

    public static boolean isEmpty(byte[] bytes) {
        return bytes == null || bytes.length == 0;
    }

    public static boolean isNull(Object obj) {
        return obj == null;
    }

    public static boolean isEmptyList(List list) {
        return list == null || list.isEmpty();
    }

    public static boolean isEmptyMap(HashMap hashMap) {
        return hashMap == null || hashMap.isEmpty();
    }
}
