package nat.com.vn.rxandroid.ui.base.listeners;

public interface ErrorView {
    void showError(String errorMessage);
}
