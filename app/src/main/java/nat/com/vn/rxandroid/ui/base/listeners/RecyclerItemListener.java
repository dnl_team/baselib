package nat.com.vn.rxandroid.ui.base.listeners;

public interface RecyclerItemListener {
    void onItemSelected(int position);
}
